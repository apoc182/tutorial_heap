import random
from time import sleep


class HeapNode(object):
    def __init__(self, priority, value):
        self.priority = priority
        self.value = value        

    def __repr__(self):
        return "Priority: " + str(self.priority) + ", Value: " + str(self.value)

    def __lt__(self, other):
        return self.priority < other.priority
    
    def __gt__(self, other):
        return self.priority > other.priority
    
    def __eq__(self, other):
        return self.priority == other.priority

class Heap(object):
    def __init__(self):
        self.heap = [] # Initialise an array (list) for storing the heap.
        self.current_index = -1 # Begin from negative one

    def add(self, node:HeapNode):
        self.heap.append(node)
        self.current_index += 1
        self.sort_up(self.current_index)

    def __len__(self):
        return self.current_index + 1

    def peak(self):
        return None if self.current_index == -1 else self.heap[0]

    def pop(self):
        if self.current_index == 0:
            self.current_index -= 1
            return self.heap.pop(0)

        if self.current_index > 0:
            node = self.heap[0]
            self.heap[0] = self.heap.pop(-1)
            self.current_index -= 1
            self.sort_down(0)
            return node
        return None

    # 2p + 1 = left child.
    # 2p + 2 = right child.
    # Parent (n-1)/2 floored.

    def sort_up(self, index):
        while index > 0:
            parent_index = (index - 1) // 2

            if self.heap[index].priority < self.heap[parent_index].priority:                
                self.swap(index, parent_index)
                index = parent_index
            else:
                break

    def sort_down(self, index):
        

        while index <= self.current_index:
            

            # Need to get the smallest.
            smallest_child_index = self.get_smallest_child(index)

            if smallest_child_index > self.current_index:
                break

            if self.heap[index].priority >= self.heap[smallest_child_index].priority:
                self.swap(smallest_child_index, index)
                index = smallest_child_index          
            else:
                break

    def get_smallest_child(self, index):
        left_child_index = (index * 2) + 1
        right_child_index = (index * 2) + 2

        if right_child_index > self.current_index:
            return left_child_index

        if self.heap[left_child_index].priority > self.heap[right_child_index].priority:
            return right_child_index
        else:
            return left_child_index

    def varify_tree(self):
        for index in range(self.current_index):
            left_child_index = (index * 2) + 1
            right_child_index = (index * 2) + 2

            if right_child_index > self.current_index:
                if self.heap[left_child_index].priority < self.heap[index].priority:
                    print(self.heap[left_child_index].value + " is smaller than " + self.heap[index].value)
                    return False
                else:
                    return True

            if self.heap[left_child_index].priority < self.heap[index].priority:
                print(self.heap[left_child_index].value + " at index " + str(left_child_index) + " is smaller than " + self.heap[index].value)
                return False

            if self.heap[right_child_index].priority < self.heap[index].priority:
                print(self.heap[right_child_index].value + " at index " + str(right_child_index) + " is smaller than " + self.heap[index].value)
                return False
            
                
    def swap(self, index_a, index_b):
        tmp = self.heap[index_a]
        self.heap[index_a] = self.heap[index_b]
        self.heap[index_b] = tmp

    def __repr__(self):

        op = ""
        for index, node in enumerate(self.heap):
            op += str(node) + " "
            


        return op
