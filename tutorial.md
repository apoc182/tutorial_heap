
- My name Justin from AntiType
- Today we are going to be making a head data structure.
- A heap is allows us to maintain order based on comparisons in a list.
- The use case I first encountered for this was in the A* search algorithm which i may cover in a later video.
- Python already has a heap implementation called heapq but the point of this is to understand it and perhaps
  write your own for specific use cases if needed.
- I am by no means perfectly correct about all of this so if you see anything that is wrong, please feel free to comment.
- In the tests, we can see the python inbuilt heapify is the fastest so I would use that in a practical sense. This was made by people that know more than me and I tried to keep this version understandable over efficient.

Tutorial
- So we have a situation where we need to get the next lowest item in a list.
- Let's say we have a city and the Mayor has decided to go about the place fixing all the derelict buildings. A crazy scenario, i know, but bear with me. He has engineers all over the city inspecting buildings and adding a score from zero to ten for the condition: 0 being bad and 10 being fantastic. The city is huge so the list will be massive. Let's say, 10,000 buildings. So efficiency is priority. Let's take a look at a few basic solutions to this problem.

