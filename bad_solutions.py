import random
from faker import Faker
import time
from heap import Heap, HeapNode
from heapq import *

# SOLUTION 1. List.
address_gen = Faker()

buildings = []


def init():
    for i in range(0,10):
        buildings.append((random.randint(0,10), address_gen.address()))


def get_all_smallest():
    op = []
      
    while len(buildings) > 0: 
        smallest = (-1, "Dummy")
        small_index = 0      
        for index, building in enumerate(buildings):
            if smallest[0] <= building[0]:
                smallest = building
                small_index = index
        op.insert(0, smallest)
        del buildings[small_index]
                
    return op

def built_in_sort():
    
    def s(x):
        return x[0]

    buildings.sort(key=s)

def test_heap(heap_implementation, qty_multiplier):

    # Run 1000 times and take the average execution time.
    sum = 0
    times_to_run = 50

    for i in range(0,times_to_run):
        t0 = time.time()
        for i in range(0,100 * qty_multiplier):
            heap_implementation.add(HeapNode(i, str(i)))
        
        for i in range(0, 10 * qty_multiplier):
            heap_implementation.pop()

        for i in range(0,100 * qty_multiplier):
            heap_implementation.add(HeapNode(i, str(i)))

        for i in range(0, 190 * qty_multiplier):
            heap_implementation.pop()

        t1 = time.time()

        sum += t1-t0

    return sum/times_to_run

class ListMinimumsOrderAsGo(object):
    def __init__(self):
        self.items = []

    def pop(self):
        return self.items.pop(0)

    def peak(self):
        return self.items[0]

    def add(self, something):
        for index, item in enumerate(self.items):
            if item > something:        
                self.items.insert(index - 1, something)
                return
        self.items.append(something)


class ListMinimums(object):

    def __init__(self):
        self.items = []

    def pop(self):
        return self.items.pop(self.find_smallest_index())

    def peak(self):
        return self.items[self.find_smallest_index()]

    def add(self, something):
        self.items.append(something)

    def find_smallest_index(self):
        smallest = None
        smallest_index = None
        for index, item in enumerate(self.items):
            if smallest is None or item < smallest:
                smallest = item
                smallest_index = index
        
        return smallest_index


class HeapqWrapper(object):
    def __init__(self):
        self.items = []
        heapify(self.items)

    def pop(self):
        return heappop(self.items)

    def add(self, something):
        heappush(self.items, something)


def execute_increasing_tests(heap):
    op = "Results: \n"
    for i in range(1,10):
        op += str(test_heap(heap, i)) + " at level " + str(i) + "\n"
    return op



if __name__ == '__main__':
    test_us = [ListMinimums(), ListMinimumsOrderAsGo(), Heap(), HeapqWrapper()]
    for test in test_us:
        print(execute_increasing_tests(test))
    
    
    
